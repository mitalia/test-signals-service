#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>

pid_t gettid(void) {
    return syscall(SYS_gettid);
}

void write_tid(const char *header) {
    char buf[512] = "";
    char tid_buf[64] = "";
    strncat(buf, header, sizeof(buf) - sizeof(tid_buf));
    pid_t tid = gettid();
    char *e = buf + sizeof(buf) - 1;
    *e-- = 0;
    *e-- = '\n';
    do {
        *e-- = '0' + (tid % 10);
        tid /= 10;
    } while(tid != 0);
    *e-- = ' ';
    strcat(buf, e+1);
    ssize_t ret = write(1, buf, strlen(buf));
    (void)ret;
}

void sigterm_handler(int s) {
    (void)s;
    write_tid("sigterm_handler");
    sleep(10);
}

void *thread_main(void *a) {
    (void)a;
    write_tid("thread launched from main");
    while(1) sleep(256);
    return NULL;
}

int main() {
    const int nthreads = 8;
    pthread_t threads[nthreads];
    int i;
    signal(SIGTERM, &sigterm_handler);
    for(i = 0; i < nthreads; ++i) {
        if(pthread_create(&threads[i], NULL, &thread_main, NULL)) {
            perror("pthread_create");
            return 1;
        }
    }
    for(i = 0; i < nthreads; ++i) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}
