all: test-signals-service

test-signals-service: test-signals-service.c
	gcc -O3 -pthread -Wall -Wextra test-signals-service.c -otest-signals-service

clean:
	rm -f test-signals-service
